from open_fortran_parser import parse, to_graphviz

# build the XML AST
xml = parse('../test/examples/hello_world.f90', verbosity=0)

# write the AST to a Graphviz Dot file
to_graphviz(xml, './hello_world_ast.dot')
