from sys import argv


class IntPointer:
    def __init__(self, value=0):
        self.value = value


def to_graphviz(root, output_filepath=None):

    def to_graphviz_internal(root, int_ptr, accumulator=[]):
        if 'name' in root.attrib:
            accumulator.append('    %d [label="%s\n\\\"%s\\\""];\n' % (int_ptr.value, root.tag.upper(), root.attrib['name']))
        elif 'id' in root.attrib:
            accumulator.append('    %d [label="%s\n\\\"%s\\\""];\n' % (int_ptr.value, root.tag.upper(), root.attrib['id']))
        elif 'value' in root.attrib:
            accumulator.append('    %d [label="%s %s\n%s"];\n' % (int_ptr.value, root.attrib['type'], root.tag.upper(), root.attrib['value'].replace('"', '\\"')))
        else:
            accumulator.append('    %d [label="%s"];\n' % (int_ptr.value, root.tag.upper()))
        tmp = int_ptr.value
        for child in root:
            int_ptr.value += 1
            accumulator.append('    %d -> %d;\n' % (tmp, int_ptr.value))
            to_graphviz_internal(child, int_ptr, accumulator)

    strings = ['digraph AST {\n']
    to_graphviz_internal(root, IntPointer(), strings)
    strings.append('}\n')
    if output_filepath is None:
        for line in strings:
            print(line)
    else:
        with open(output_filepath, 'w') as output_file:
            for line in strings:
                output_file.write(line + '\n')
